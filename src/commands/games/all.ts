import { Command, flags } from '@oclif/command';

const puppeteer = require('puppeteer');

export default class GamesAll extends Command {
  static description = 'describe the command here';

  static flags = {
    help: flags.help({ char: 'h' }),
    name: flags.string({ char: 'n', description: 'name to print' }),
    force: flags.boolean({ char: 'f' }),
  };

  static args = [{ name: 'file' }];

  async run() {
    const { args, flags } = this.parse(GamesAll);

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 926 });
    await page.goto('http://www.conmebol.com/es/copa-libertadores-2019/fixture', { waitUntil: 'load', timeout: 0 });

    const data = await page.evaluate(() => {
      const teams = [] as {homeTeam: string, homeScore: number, awayTeam: string, awayScore: number}[];
      const matches = document.querySelectorAll('tr.Opta-Scoreline');
      matches.forEach(row => {
        teams.push({
          homeTeam: row.querySelector('Opta-Team.Opta-Home').innerText || 'oops',
          homeScore: row.querySelector('Opta-Score.Opta-Home').innerText || 'oops',
          awayTeam: row.querySelector('Opta-Team.Opta-Away').innerText || 'oops',
          awayScore: row.querySelector('Opta-Score.Opta-Away').innerText || 'oops',
        });
      });
      return teams;
    });

    this.log(data);
  }
}
