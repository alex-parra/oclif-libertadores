# oclif-libertadores
Node.js CLI with Oclif to show data of Copa dos Libertadores

==================

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
<!-- [![Version](https://img.shields.io/npm/v/oclif-libertadores.svg)](https://npmjs.org/package/oclif-libertadores) -->
<!-- [![Downloads/week](https://img.shields.io/npm/dw/oclif-libertadores.svg)](https://npmjs.org/package/oclif-libertadores) -->
<!-- [![License](https://img.shields.io/npm/l/oclif-libertadores.svg)](https://github.com/alex-parra/oclif-libertadores/blob/master/package.json) -->

<!-- toc -->
* [oclif-libertadores](#oclif-libertadores)
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g oclif-libertadores
$ oclif-libertadores COMMAND
running command...
$ oclif-libertadores (-v|--version|version)
oclif-libertadores/0.0.0 darwin-x64 node-v12.6.0
$ oclif-libertadores --help [COMMAND]
USAGE
  $ oclif-libertadores COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`oclif-libertadores games:all [FILE]`](#oclif-libertadores-gamesall-file)
* [`oclif-libertadores hello [FILE]`](#oclif-libertadores-hello-file)
* [`oclif-libertadores help [COMMAND]`](#oclif-libertadores-help-command)

## `oclif-libertadores games:all [FILE]`

describe the command here

```
USAGE
  $ oclif-libertadores games:all [FILE]

OPTIONS
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print
```

_See code: [src/commands/games/all.ts](https://github.com/alex-parra/oclif-libertadores/blob/v0.0.0/src/commands/games/all.ts)_

## `oclif-libertadores hello [FILE]`

describe the command here

```
USAGE
  $ oclif-libertadores hello [FILE]

OPTIONS
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print

EXAMPLE
  $ oclif-libertadores hello
  hello world from ./src/hello.ts!
```

_See code: [src/commands/hello.ts](https://github.com/alex-parra/oclif-libertadores/blob/v0.0.0/src/commands/hello.ts)_

## `oclif-libertadores help [COMMAND]`

display help for oclif-libertadores

```
USAGE
  $ oclif-libertadores help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.0/src/commands/help.ts)_
<!-- commandsstop -->
